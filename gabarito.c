/*
	Name: Acertos MASTER
	Copyright: JF SOFTWARE
	Author: Joao Elias Ferraz Santana
	Date: 28/10/21 10:48
	Description: Escreva um programa que leia uma lista G de 20 elementos caractere
	que representa o gabarito de uma prova com 20 questoes. A seguir, para cada um
	dos 50 alunos de uma turma, leia o nome e o vetor de respostas (R) do aluno,
	após ler conte o numero de acertos desse aluno e mostre o numero de acertos e
	uma mensagem APROVADO, se a nota FINAL for maior ou igual a 6 ou mostre o numeros
	de acertos e uma mensagem de REPROVADO, caso contrario. Cada questao correta vale
	0,5 ponto.
*/

#include <stdio.h>
#include <stdlib.h>
#define NUMQUEST 20
#define NUMALUNO 50

void exibeInstrucao(void);

int main(int argc, char const *argv[])
{
    char gabarito[NUMQUEST], respAluno[NUMALUNO][NUMQUEST];
    int i, j, contAcerto=0;
    exibeInstrucao();
    for(i=0; i<NUMQUEST; i++){
        system("cls");
        printf("\t\t\t<<<CADASTRO DE QUESTAO>>>\n\n\n\t\t\t\tINSERCAO DE RESPOSTAS\n\n\t\t\tQUESTAO %d>>", i+1);
        fflush(stdin);
        gabarito[i] = getchar();
        system("cls");
    }
    for (i = 0; i < NUMALUNO; i++)
    {
        printf("\t\t\t<<<ALUNO %d>>>\n\n", i+1);
        for (j = 0; j < NUMQUEST; j++)
        {
            printf("\t\t\tRESPOSTA DA QUESTAO %d>>", j+1);
            fflush(stdin);
            respAluno[i][j] = getchar();
            if (respAluno[i][j] == gabarito[j])
            {
                contAcerto++;
            }
        }
        if (contAcerto>=12)
        {
            printf("\t\t\tAPROVADO!\n\n");
        }
        else
        {
            printf("\t\t\tREPROVADO!\n\n");
        }
        contAcerto = 0;
        system("pause");
        system("cls");
    }
    
    return 0;
}

void exibeInstrucao(void){
    printf("\t\t\t\tINSTRUCOES DE USO DO PROGRAMA\n\n");
    printf("\tProfessor, voce ira cadastrar as respostas da prova e, logo em seguida, ira inserir\n");
    printf("\tas respostas das questoes dos alunos conforme a ordem da CADERNETA. Assim, o aluno 1 e\n");
    printf("\to PRIMEIRO ALUNO da CHAMADA.\n\n");
    system("pause");
}
