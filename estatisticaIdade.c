/*
	Name: Estatisticas Etarias
	Copyright: JF Software LTDA
	Author: Joao Elias Ferraz Santana
	Date: 10/10/21 10:44
	Description:
	Faca um programa que receba a idade de 15 pessoas e calcule e mostre:
		--a quantidade de pessoas em cada faixa etaria
		--a percentagem de pessoas na primeira e na ultima faixa etaria, com
		relacao ao total de pessoas.
*/

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
int main(){
	int idade, cont; float perc1=0, perc5=0, cont1=0, cont2=0, cont3=0, cont4=0, cont5=0;
	setlocale(LC_ALL, "Portuguese");
	for(cont=1; cont<=15; cont++){
		printf("Informe a sua idade: ");
		scanf("%d", &idade);
		if(idade<=15){cont1++;}
		if(idade>=16 && idade<=30){cont2++;}
		if(idade>=31 && idade<=45){cont3++;}
		if(idade>=46 && idade<=60){cont4++;}
		if(idade>=61){cont5++;}
	}
	perc1=cont1/15; perc5=cont5/15;
	printf("\n********************RESULTADOS********************\nPessoas na 1� faixa Et�ria: %.0f\nPessoas na 2� faixa et�ria: %.0f\nPessoas na 3� faixa et�ria: %.0f\nPessoas na 4� faixa et�ria: %.0f\nPessoas na 5� faixa et�ria: %.0f\n", cont1, cont2, cont3, cont4, cont5);
	printf("Percentual de passoas na 1� faixa et�ria: %.2f %%\nPercentual de pessoas na 5� faixa et�ria: %.2f %%\n", 100*perc1, 100*perc5);
	system("pause"); return 0;
}
