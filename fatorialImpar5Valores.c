/*
	Name: Fatorial Impar
	Copyright: JF SOFTWARE LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 13/10/21 16:07
	Description: Calcula o fatorial de um numero impar
*/

/*
Modificar o código para que receba um valor menor que 15
do teclado e exibir em cada linha, o fatorial dos 5 impares
anteriores ao numero informado.
*/

#include <stdio.h>

int main()
{
    int num, i, fatorial=1, contador=0, valor, verdade = 1;
    while(verdade == 1){
        printf("Digite um valor menor que 15: ");
        scanf("%d", &valor);
        if(valor<15){
            break;
        }
    }
    for(num=valor; num>=1; num--){
        if(num % 2 !=0){
            for(i=num; i>=1; i--){
                fatorial = fatorial * i;
            }
            printf("Fatorial de %d: %d\n", num, fatorial);
            fatorial = 1;
            contador++;
            if(contador == 5){
                break;
            }
        }
    }
    return 0;
}
