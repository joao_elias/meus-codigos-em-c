#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
int main(void){
	int cod=0;
	float preco=0, qtd=0; char op='s';
	setlocale(LC_ALL, "Portuguese");
	do{
		printf("**********Menu de op��es**********\n\n100 - Cachorro quente\n101 - Bauru simples\n102 - Bauru com ovo\n103 - Hamb�rger\n104 - Cheeseburguer\n105 - Refrigerante\nEntrada: ");
		scanf("%d", &cod);
		if(cod==100){
			printf("Cachorro quente.\nInforme a quantidade: ");
			scanf("%f", &qtd);
			preco=preco+qtd*1.2;
		}
		if(cod==101){
			printf("Bauru simples.\nInforme a quantidade: ");
			scanf("%f", &qtd);
			preco=preco+qtd*1.3;
		}
		if(cod==102){
			printf("Bauru com ovo.\nInforme a quantidade: ");
			scanf("%f", &qtd);
			preco=preco+qtd*1.5;
		}
		if(cod==103){
			printf("Hamb�rger.\nInforme a quantidade: ");
			scanf("%f", &qtd);
			preco=preco+qtd*1.2;
		}
		if(cod==104){
			printf("Cheeseburguer.\nInforme a quantidade: ");
			scanf("%f", &qtd);
			preco=preco+qtd*1.3;
		}
		if(cod==105){
			printf("Refrigerante.\nInforme a quantidade: ");
			scanf("%f", &qtd);
			preco=preco+qtd*1;
		}
		printf("Deseja algo a mais?\nEntrada: ");
		scanf(" %c", &op);
	}while(op=='s'||op=='S');
	printf("Total a pagar: R$%.2f\n", preco);
	system("pause"); return 0;
}
