/*
	Name: Vetor Automatico
	Copyright: JF Softare LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 18/10/21 15:07
	Description: O usuario entra com dados em um vetor e insere
    valores em outro vetor. Criterio de insercao:
	Indice par: o valor de B sera 5 vezes o valor de A
	Indice impar: o valor de B sera o de A somado com 5.
*/

#include <stdio.h>
#define TAM 10

int main(void){
	int i, A[TAM], B[TAM];
	for(i=0; i<TAM; i++){
        printf("Elemento [%d] pra matriz A: ", i);
        scanf("%d", &A[i]);
		if(i%2 == 0){
            B[i] = 5*A[i];
        }
        else{
            B[i] = A[i] + 5;
        }
    }
    printf("Elementos do vetor:\n");
    for(i=0; i<TAM; i++){
        printf("%d ", B[i]);
    }
    return 0;
}
