#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#define MAX 255
#define MAXCARROS 25840
typedef struct{
    char placa[MAX], marca[MAX], modelo[MAX];
    int ano;
}Carro;

void incluir(void);
void alterar(int indice);
void imprimir(void);
void imprimirAno(int anoMin, int anoMax);
void limpaTela(void);

Carro *lista;
int i=0, numCarros; //contador para inclus�o de carros

int main(void){setlocale(LC_ALL, "Portuguese");
    lista = (Carro *) malloc(MAXCARROS * sizeof(Carro));
    char op='0', placa[MAX];
    int indice, anoMin, anoMax;
    while(op!='6'){
        printf("\t\t\t\tMENU PRINCIPAL\n\n\n");
        printf("\t1.\tCadastrar as informa��es de ve�culo (%d espa�os dispon�veis de %d)\n", (MAXCARROS-numCarros), MAXCARROS);
        printf("\t2.\tAlterar as informa��es de ve�culo\n");
        printf("\t3.\tImprimir por ano\n");
        printf("\t4.\tPesquisar ve�culo por placa\n");
        printf("\t5.\tListar todos os ve�culos cadastrados\n");
        printf("\t6.\tSair da aplica��o\n\n");
        printf("\tPara escolher uma op��o, escolha o N�MERO CORRESPONDENTE\n\n");
        printf("\tEscolha: ");
        fflush(stdin); //limpa o buffer
        op=getchar();
        system("cls");
        switch(op){
            case '1':
                incluir();
                break;
            case '2': printf("Entre com o �ndice: ");
                scanf("%d", &indice);
                alterar(indice);
                break;
            case '3':
                printf("Entre com o ano m�nimo: ");
                scanf("%d", &anoMin);
                printf("Entre com o ano m�ximo: ");
                scanf("%d", &anoMax);
                imprimirAno(anoMin, anoMax);
                break;
            case '4':
                printf("Entre com a placa: ");
                fflush(stdin);
                gets(placa);
                break;
            case '5':
                imprimir();
                break;
            case 6: break;
        }
    } free(lista);
    return 0;
}
void limpaTela(void){
    system("pause");
    system("cls");
}
void incluir(void){
    char add='s'; //inicializa��o para entrar no loop while.
    while(add=='s'||add=='S'){
        if(i>=MAXCARROS){
            printf("\n\tImposs�vel adicionar mais carros!\n\tLista cheia.\n\n\t");
            limpaTela();
            break;
        }
        printf("Carro %d\n\n", i+1);
        printf("Placa: ");
        fflush(stdin);
        gets(lista[i].placa);
        printf("Marca: ");
        fflush(stdin);
        gets(lista[i].marca);
        printf("Modelo: ");
        fflush(stdin);
        gets(lista[i].modelo);
        printf("Ano: ");
        scanf("%d", &lista[i].ano);
        printf("Mais algum carro? (S/N)  ");
        fflush(stdin);
        add=getchar();
        numCarros=i+1; //incrementa o n�mero de carros a partir do contador
        i++;
        if(add=='N'||add=='n'){
            break;
        }
        system("cls");
    }
    limpaTela();
}

void alterar(int indice){
    if(lista[indice].placa[0]=='\0'){
        printf("Nesta posi��o, n�o h� carros cadastrados!\n\n");}
    else{
        printf("Placa: ");
        fflush(stdin);
        gets(lista[indice].placa);
        printf("Marca: ");
        fflush(stdin);
        gets(lista[indice].marca);
        printf("Modelo: ");
        fflush(stdin);
        gets(lista[indice].modelo);
        printf("Ano: ");
        scanf("%d", &lista[indice].ano);}
        limpaTela();
}

void imprimirAno(int anoMin, int anoMax){int j;
    for(j=0; j<numCarros; j++){
        if(lista[j].ano>=anoMin&&lista[j].ano<=anoMax){
            printf("Placa: ");
            puts(lista[j].placa);
            printf("\nMarca: ");
            puts(lista[j].marca);
            printf("\nModelo: ");
            puts(lista[j].modelo);
            printf("\nAno: %d\n\n", lista[j].ano);
            break;
        }
    }
    limpaTela();
}

void imprimir(void){int j;
    for(j=0; j<numCarros; j++){
        printf("Placa: ");
        puts(lista[j].placa);
        printf("\nMarca: ");
        puts(lista[j].marca);
        printf("\nModelo: ");
        puts(lista[j].modelo);
        printf("\nAno: %d\n\n", lista[j].ano);
    }
    limpaTela();
}

void imprimirPlaca(char placa[MAX]){
    int j, logico, posicao;
    for(j=0; j<numCarros; j++){
        if(strcmp(lista[i].placa,placa)){
            logico=1;
            posicao=j;
            break;
        }
        else{
            logico=0;
        }
    }
    if(logico==1){
           printf("Placa: ");
           puts(lista[posicao].placa);
           printf("\nMarca: ");
           puts(lista[posicao].marca);
           printf("\nModelo: ");
           puts(lista[posicao].modelo);
           printf("\nAno: %d\n\n", lista[posicao].ano);
    }
    else{
        printf("O carro n�o existe na base de dados!\n\n");
    }
    limpaTela();
}
