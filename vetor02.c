/*
	Name: Leitura e atribuicao
	Copyright: JF Software LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 18/10/21 15:42
	Description: Le 8 valores do teclado, guarda em uma matriz
	e coloca em outra matriz triplicando os mesmos valores.
*/


#include <stdio.h>
#include <stdlib.h>
#define TAM 8

int main(void){
	int i, A[TAM], B[TAM];
	for(i=0; i<TAM; i++){
		printf("Elemento [%d]>> ", i);
		scanf("%d", &A[i]);
		B[i] = A[i]*3;
	}
	printf("VETOR B:\n[ ");
	for(i=0; i<TAM; i++){
		printf("%d ", B[i]);
	}
	printf("]\n\n");
	system("pause");
	return 0;
}
