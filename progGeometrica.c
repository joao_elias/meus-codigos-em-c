/*
	Name: Progressao Geometrica
	Copyright: JF SOFTWARE LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 05/11/21 09:09
	Description: Este programa calcula os termos da PG, retorna a soma dos termos
	e classifica a PG em crescente, decrescente, oscilante e constante. O mesmo
	se utiliza de recursividade
*/

#include <stdio.h>

void progGeom(double termo1, double razao, double numTermos);
double potencia(double base, double exp);
void classificaPG(double termo1, double razao);

int main()
{
    double a1, q, n, somaPG;
    printf("Digite o primeiro termo>>");
    scanf("%lf", &a1);
    printf("Dgite a razao>>");
    scanf("%lf", &q);
    printf("Digite o numero de termos>>");
    scanf("%lf", &n);
    progGeom(a1, q, n-1);
    classificaPG(a1, q);
    somaPG = (a1*(potencia(q,n)-1))/(q-1);
    printf("\nSOMS DOS TERMOS DA P.G.: %0.lf\n", somaPG);
    return 0;
}

void progGeom(double termo1, double razao, double numTermos){
    if(numTermos == 1){
    	printf("Termo>>%.0lf", termo1*razao);
    	return; //aqui, a funcao eh encerrada
	}
	printf("Termo>>%.0lf\n", termo1*potencia(razao, numTermos)); //calculo do termo recursivamente.
    progGeom(termo1, razao, numTermos-1); //chamada da funcao
}

double potencia(double base, double exp){
	//potenciacao recursiva
    if (exp == 1)
    {
        return base;
    }
    if (exp == 0)
    {
        return 1;
    }
    return base*potencia(base, exp-1);
}

void classificaPG(double termo1, double razao){
	//classificacao da PG
	if(razao>0 && termo1>0){
		if(razao!=1){
			printf("\nPG Crescente!\n");
		}
		else{
			printf("\nPG Constante!\n");
		}
	}
	else if(razao>0 && termo1<0){
		printf("\nPG Decrescente!\n");
	}
	else if(razao<0){
		printf("\nPG Oscilante!\n");
	}
}
