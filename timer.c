#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

int main(){
	int segundos, minutos, horas;
	for(horas = 0; horas<24; horas++){
		for(minutos = 0; minutos<60; minutos++){
			for(segundos = 0; segundos<60; segundos++){
				printf("%d:%d:%d\n", horas, minutos, segundos);
				Sleep(1000);
			}
		}
	}
	system("pause");
	return 0;
}
