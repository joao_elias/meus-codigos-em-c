#include <stdio.h>
#include <stdlib.h>

void main(void){
    int TAM, i;
    double *vct;
    printf("Digite quantos termos vc quer ver em Fibonacci: ");
    scanf("%d", &TAM);
    vct = (double *)malloc(TAM*sizeof(double));
    vct[0]=1;
    vct[1]=1;
    for(i=2; i<TAM; i++){
        vct[i]=vct[i-2]+vct[i-1];
    }
    system("cls");
    printf("Termos da sequencia:\n");
    for(i=0; i<TAM; i++){
        printf("%.0lf ", vct[i]);
    }
    printf("\n\n");
    system("pause");
}
