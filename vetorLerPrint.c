/*
	Name: Leitura
	Copyright: JF Software LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 18/10/21 15:42
	Description: Le 10 valores do teclado e imprime-os
*/


#include <stdio.h>
#include <stdlib.h>
#define TAM 10

int main(void){
	int i, vetor[TAM];
	for(i=0; i<TAM; i++){
		printf("Elemento [%d]>> ", i);
		scanf("%d", &vetor[i]);
	}
	printf("VETOR:\n[ ");
	for(i=0; i<TAM; i++){
		printf("%d ", vetor[i]);
	}
	printf(" ]\n\n");
	system("pause");
	return 0;
}
