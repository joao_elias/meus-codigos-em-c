/*
	Name: Potencia Recursiva
	Copyright: 
	Author: 
	Date: 03/11/21 22:00
	Description: Implemente uma funcao recursiva que,
    dados dois numeros inteiros x e n, calcula o valor
    de x elevado a n.
*/

#include <stdio.h>

double potencia(double base, double exp);

int main(int argc, char const *argv[])
{
    double val, exp;
    printf("Numero base>> ");
    scanf("%lf", &val);
    printf("Numero expoente>> ");
    scanf("%lf", &exp);
    printf("%.1lf elevado a %0.lf = %.3lf\n\n", val, exp, potencia(val, exp));
    return 0;
}

double potencia(double base, double exp){
    if (exp == 1)
    {
        return base;
    }
    if (exp == 0)
    {
        return 1;
    }
    return base*potencia(base, exp-1);
}
