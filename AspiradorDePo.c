#include <stdio.h>
#include <stdlib.h>// necessário p/ as funções rand() e srand()
#include <time.h>//necessário p/ função time()

void pause (float);

int main()
{
    srand(time(NULL));
    char ambiente[10][10];
    
    int i, j, posX, posY, posExtra;
    
    //geracao do ambiente limpo para remover lixo de memoria
    
    for(i=0; i<10; i++){
        for(j=0; j<10; j++){
            ambiente[i][j] = ' ';
        }
    }
    
    //geracao do ambiente com poeira
    for(i=0; i<10; i++){
        for(j=0; j<10; j++){
            posX = rand() % 10;
            posY = rand() % 10;
            ambiente[posX][posY] = '.';
        }
    }
    
    //geracao do ambiente com moveis
    for(i=0; i<3; i++){
        posX = rand() % 10;
        posY = rand() % 10;
        ambiente[posX][posY] = 'x';
    }
    
    printf("\n\tSituacao atual da casa: (legenda-> '.' = sujo; 'x' = movel; (nada) = limpo.)\n\n");
    
    for(i=0; i<10; i++){
        for(j=0; j<10; j++){
            printf("%c ", ambiente[i][j]);
        }
        printf("\n");
    }
    
    printf("Vamos limpar!\n\n");
    
    for(i=0; i<10; i++){
    	for(j=0; j<10; j++){
    		if(ambiente[i][j] == '.'){
    			printf("Ambiente sujo!\nLimpando...\n");
    			pause(5);
    			ambiente[i][j] = ' ';
    			printf("\nPronto! Ambiente Limpo!\n");
			}
			else if(ambiente[i][j] == ' '){
				printf("Ambiente limpo!\nIndo para a direita...\n");
			}
			else if(ambiente[i][j] == 'x'){
				printf("Obstaculo detectado!\nIndo para a esquerda...\n");
			}
		}
	}
	
	printf("Limpo!\n\n");
	
	for(i=0; i<10; i++){
        for(j=0; j<10; j++){
            printf("%c ", ambiente[i][j]);
        }
        printf("\n");
    }
    
    printf("Terminei meu servico!! Ufa! Hora do descanso!!\n\n\nAperte <enter> para me desligar...");
    fflush(stdin);
    getchar();
    
    return 0;
}

void pause (float delay1) {
	/** procedimento de compilacao normal
	* uso: pause(valor)
	*
	* valor eh um numero maior que zero podendo ser uma fracao.
	* eh o tempo em segundos de pausa no programa.
	**/
   
   if (delay1<0.001) return; // pode ser ajustado e/ou evita-se valores negativos.

   float inst1=0, inst2=0;

   inst1 = (float)clock()/(float)CLOCKS_PER_SEC;

   while (inst2-inst1<delay1) inst2 = (float)clock()/(float)CLOCKS_PER_SEC;

   return;

}
