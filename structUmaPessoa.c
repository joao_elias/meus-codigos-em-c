/*
	Name: Cadastro de uma Pessoa
	Copyright: 
	Author: Joao Elias Ferraz Santana
	Date: 19/11/21 17:07
	Description: fazer um programa que leia o nome a idade e o endereco de uma pessoa 
	e armazene em uma estrutura. Depois mostre na tela todos os dados armazenados
*/


#include <stdio.h>
#include <stdlib.h>

typedef struct{
    char CEP[8];
    char rua[255];//
    char bairro[125];//
    char cidade[80];//
    char estado[30];//
    int num;//
}Endereco;

typedef struct{
    char nome[255];
    int idade;
    Endereco local;
}Pessoa;

Pessoa inputData();
void imprimeDados(Pessoa dadosPessoa);

int main()
{
    Pessoa dados = inputData();
    imprimeDados(dados);
    system("pause");
	return 0;
}

Pessoa inputData(){
	Pessoa retornoFuncao;
	printf("\t\t\t\t\tENTRADA DE DADOS\n\n\n");
    printf("Seu nome>>");
    fflush(stdin);
    gets(retornoFuncao.nome);
    printf("Sua idade>>");
    scanf("%d", &retornoFuncao.idade);
    printf("Rua>>");
    fflush(stdin);
    gets(retornoFuncao.local.rua);
    printf("Numero>>");
    scanf("%d", &retornoFuncao.local.num);
    printf("Bairro>>");
    fflush(stdin);
    gets(retornoFuncao.local.bairro);
    printf("Cidade>>");
    fflush(stdin);
    gets(retornoFuncao.local.cidade);
    printf("Estado>>");
    fflush(stdin);
    gets(retornoFuncao.local.estado);
    printf("CEP>>");
    fflush(stdin);
    gets(retornoFuncao.local.CEP);
    system("cls");
    return retornoFuncao;
}

void imprimeDados(Pessoa dadosPessoa){
	printf("\t\t\t\t\tDADOS INFORMADOS PELO USUARIO\n\n\n");
	printf("\t\tNOME: %s\n", dadosPessoa.nome);
    printf("\t\tIDADE: %d\n", dadosPessoa.idade);
    printf("\t\tENDERECO\n\n\t\tRua: %s\n", dadosPessoa.local.rua);
    printf("\t\tNumero: %d\n", dadosPessoa.local.num);
    printf("\t\tBairro: %s\n", dadosPessoa.local.bairro);
    printf("\t\tCidade: %s\n", dadosPessoa.local.cidade);
    printf("\t\tEstado: %s\n", dadosPessoa.local.estado);
    printf("\t\tCEP: %s\n", dadosPessoa.local.CEP);
    int i;
	for(i=1; i<=70; i++){
		printf("*");
	}
	printf("\n\n");
}
