/*
	Name: Programa de Matrizes
	Copyright: JF Software LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 23/09/21 15:14
	Description: Fazer um programa em C que leia do teclado 10 n�meros e armazene numa matriz,
	depois imprima cada n�mero digitado em �rdem inversa, ou seja, do �ltimo at� o primeiro.
	P.S.  A matriz n�o deve ser alterada, apenas a sua exibi��o.
*/

#include <stdio.h>
#include <stdlib.h>
#define QUANTIDADE 10

int main(void){
	int numeros[QUANTIDADE], i, j; //declaracao de variaveis
	for(i=0; i<QUANTIDADE; i++){
		printf("Valor [%d]: ", i+1);
		scanf("%d", &numeros[i]); //leitura do teclado
		system("cls");
	}
	printf("Valores na ordem INVERSA:\n\n");
	for(i=QUANTIDADE-1; i>=0; i--){
		printf("%d ", numeros[i]);
	}
	printf("\n\nFim do programa!\n\n");
	system("pause"); //espera o usuario digitar enter
	return 0;
}
