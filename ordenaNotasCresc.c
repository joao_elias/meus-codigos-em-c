/*
	Name: Calcula Media
	Copyright: JF SOFTWARE LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 29/09/21 18:54
	Description: Desenvolver um programa que efetua a leitura dos nomes de 5 alunos e tambem de suas
    duas notas semestrais. Ao final devera ser apresentado o nome de cada aluno classificado em ordem
    numerica crescente de suas medias anuais
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NUMALUNO 5

typedef struct
{
    char nome[255];
    float notas[2], media;
}Aluno;

void main(void){
    Aluno lista[NUMALUNO];
    int i, j;
    //variaveis auxiliares para os campos de aluno
    float mediaAux=0, notasAux[2];
    char nomeAux[255];
    //fim var aux
    for(i=0; i<NUMALUNO; i++){
        printf("Nome do aluno: ");
        fflush(stdin);
        gets(lista[i].nome);
        for(j=0; j<2; j++){
            printf("Nota da AV%d: ", j+1);
            scanf("%f", &lista[i].notas[j]);
        }
        lista[i].media = (lista[i].notas[0] + lista[i].notas[1]) / 2; //calculo da media
    }
    for (i = 0; i < NUMALUNO; i++)
    {
        for (j = i+1; j < NUMALUNO; j++)
        {
            if (lista[i].media > lista[j].media)
            {
                //uso de var aux
                mediaAux = lista[i].media;
                notasAux[0] = lista[i].notas[0];
                notasAux[1] = lista[i].notas[1];
                strcpy(nomeAux, lista[i].nome); //strcpy (string_destino,string_origem);
                //fim de var aux
                lista[i].media = lista[j].media;
                lista[i].notas[0] = lista[j].notas[0];
                lista[i].notas[1] = lista[j].notas[1];
                strcpy(lista[i].nome,lista[j].nome);
                lista[j].media = mediaAux;
                lista[j].notas[0] = notasAux[0];
                lista[j].notas[1] = notasAux[1];
                strcpy(lista[j].nome, nomeAux);
            }
        }
    }
    system("cls");
    printf("EXIBINDO OS ALUNOS COM MEDIA EM ORDEM CRESCENTE:\n\n");
    for (i = 0; i < NUMALUNO; i++)
    {
        printf("NOME: %s\n", lista[i].nome);
        printf("MEDIA: %.1f\n\n", lista[i].media);
    }
    system("pause");
}