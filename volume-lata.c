/*
Disciplina   : [Linguagem e Lógica de Programação]
Professor   : Orlando Vicente de Oliveira Filho
Descrição   : Calculo do volume da lata de oleo
Autor(a)    : Joao Elias Ferraz Santana
Data atual  : 08/09/2021
*/

#include <stdio.h>
#include <stdlib.h>
#define PI 3.1415926535

int main(void){
// Seção de Comandos, procedimento, funções, operadores, etc... 
    float volume, raio, altura;
    printf("Calularemos o volume da lata. Insira, NESTA ORDEM:\n");
    printf("RAIO\n");
    printf("ALTURA\n\n");
    scanf("%f%f", &raio, &altura);
    volume=PI*raio*raio*altura;
    printf("VOLUME: %.2f\n\n", volume);
    system("pause");
    return 0;
}
