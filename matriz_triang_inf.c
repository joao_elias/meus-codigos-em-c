/*Escreva um programa lˆe uma matriz e depois verifica se esta ´e
uma matriz triangular inferior.*/

#include <stdio.h>
#include <stdlib.h>

void main(void){
    int ordem, i, j, triangInf=0;
    printf("Qual a ordem da matriz? ");
    scanf("%d", &ordem);
    int matriz[ordem][ordem]; //cria a matriz
    for (i = 0; i < ordem; i++)
    {
        for (j = 0;j < ordem; j++)
        {
            printf("Elemento [%d][%d]: ", i+1, j+1);
            scanf("%d", &matriz[i][j]); //le cada elemento da matriz
        }
        
    }
    for (i = 0; i < ordem; i++)
    {
        for (j = 0; j < ordem; j++)
        {
            if (j>i && i+j>=3) //condicao para ser triangular inferior
            {
                if (matriz[i][j]==0)
                {
                    triangInf=1; //como se fosse booleano
                }
                
            }
            
        }
        
    }
    if (triangInf==1) //verifica se e verdade
    {
        printf("A matriz informada e triangular inferior!\n\n");
    }
    else //nao e triangular inferior
    {
        printf("A matriz nao e triangular inferior!\n\n");
    }
    system("pause");
}