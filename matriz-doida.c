/*
Faça um programa que devera permitir que o usuario entre com os valores dos elementos de uma 
matriz quadrada de ordem 4 e possibilite o usuario realizar as seguintes funcionalidades:
a) Imprimir todos os elementos da matriz;  (OK)
b) Somar os quadrados de todos os elementos da primeira coluna; (OK)
c) Somar todos os elementos da terceira linha; (OK)
d) Somar os elementos da diagonal principal; (OK)
e) Somar todos os elementos de indice par da segunda linha. (OK)
*/
#include <stdio.h>
#include <math.h>
#define ORDEM 4

void main(void)
{
    int matriz[ORDEM][ORDEM], i, j, somaCol1=0, somaLin3=0, somaDiagPrin=0, somaIndiParLinha2=0;
    for (i = 0; i < ORDEM; i++)
    {
        for (j = 0; j < ORDEM; j++)
        {
            printf("Elemento [%d][%d]: ", i+1, j+1);
            scanf("%d", &matriz[i][j]);
        }
        
    }
    system("cls");
    printf("TODOS OS ELEMENTOS DA MATRIZ:\n\n");
    for (i = 0; i < ORDEM; i++)
    {
        for (j = 0; j < ORDEM; j++)
        {
            printf("%d ", matriz[i][j]); //Imprimir todos os elementos da matriz
        }
        printf("\n");
        
    }
    for (i = 0; i < ORDEM; i++)
    {
        for (j = 0; j < ORDEM; j++)
        {
            if (i>=j && j==0)
            {
                somaCol1 = somaCol1 + pow(matriz[i][j], 2); //Somar os quadrados de todos os elementos da primeira coluna
            }
            
            if (i==2)
            {
                somaLin3 = somaLin3 + matriz[i][j]; //Somar todos os elementos da terceira linha
            }

            if (i==j)
            {
                somaDiagPrin = somaDiagPrin + matriz[i][j]; //Somar os elementos da diagonal principal;
            }

            if (i==1 && j%2 == 0)
            {
                somaIndiParLinha2 = somaIndiParLinha2 + matriz[i][j]; //Somar todos os elementos de índice par da segunda linha
            }
            
            
        }
        
    }
    printf("\n\nSOMA DOS QUADRADOS DOS ELEMENTOS DA 1 COLUNA: %d\n", somaCol1);
    printf("\nSOMA DOS ELEMENTOS DA 3 LINHA: %d\n", somaLin3);
    printf("\nSOMA DOS ELEMENTOS DA DIAG. PRINCIPAL: %d\n", somaDiagPrin);
    printf("\nSOMA DOS ELEMENTOS DE INDICE PAR DA LINHA 2: %d\n", somaIndiParLinha2);
    printf("\n\nPressione <ENTER> para sair...");
    fflush(stdin);
    getchar();
}