/*Escreva um programa que le todos os elementos de uma
matriz 4 × 4 e mostra a matriz e a sua transposta na tela.*/

#include <stdio.h>
#include <stdlib.h>
#define nLINHA 4
#define nCOL 4

void main(void){
    int mat[nLINHA][nCOL], i, j;
    for(i=0; i<nLINHA; i++){
        for (j = 0; j < nCOL; j++)
        {
            printf("Elemento [%d][%d]: ", i+1, j+1);
            scanf("%d", &mat[i][j]);
        }
    }
    system("cls");
    printf("Matriz informada: \n\n");
    for(i=0; i<nLINHA; i++){
        for (j = 0; j < nCOL; j++)
        {
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
    printf("\n\nMatriz transposta: \n\n");
    for(i=0; i<nLINHA; i++){
        for (j = 0; j < nCOL; j++)
        {
            printf("%d ", mat[j][i]);
        }
        printf("\n");
    }
    printf("\n\n");
    system("pause");
}