/*
	Name: Lista Encadeada
	Copyright: JF Software LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 23/09/21 21:21
	Description: Implementacao da estrutura de dados basica Lista Encadeada
*/

#include <stdlib.h>
#include <stdio.h>
#define MAX 10

/* ========================================================================= */

typedef int TipoChave;

typedef struct {
  int Chave;
  char Nome[255];
  /* outros componentes */
} TipoItem;

typedef struct TipoCelula *TipoApontador;

typedef struct TipoCelula {
  TipoItem Item;
  TipoApontador Prox;
} TipoCelula;

typedef struct {
  TipoApontador Primeiro, Ultimo;
} TipoLista;

/* ========================================================================= */

void FLVazia(TipoLista *Lista)
{ Lista -> Primeiro = (TipoApontador) malloc(sizeof(TipoCelula));
  Lista -> Ultimo = Lista -> Primeiro;
  Lista -> Primeiro -> Prox = NULL;
}

int Vazia(TipoLista Lista)
{ return (Lista.Primeiro == Lista.Ultimo);
}

void Insere(TipoItem x, TipoLista *Lista)
{ Lista -> Ultimo -> Prox = (TipoApontador) malloc(sizeof(TipoCelula));
  Lista -> Ultimo = Lista -> Ultimo -> Prox;
  Lista -> Ultimo -> Item = x;
  Lista -> Ultimo -> Prox = NULL;
}

void Retira(TipoApontador p, TipoLista *Lista, TipoItem *Item)
{ /*  ---   Obs.: o item a ser retirado e  o seguinte ao apontado por  p --- */
  TipoApontador q;
  if (Vazia(*Lista) || p == NULL || p -> Prox == NULL) 
  { printf(" Erro   Lista vazia ou posicao nao existe\n");
    return;
  }
  q = p -> Prox;
  *Item = q -> Item;
  p -> Prox = q -> Prox;
  if (p -> Prox == NULL) Lista -> Ultimo = p;
  free(q);
}

void Imprime(TipoLista Lista)
{ TipoApontador Aux;
  Aux = Lista.Primeiro -> Prox;
  printf("CHAVE NOME\n\n");
  while (Aux != NULL) 
    { printf("%d %s\n", Aux -> Item.Chave, Aux -> Item.Nome);
      Aux = Aux -> Prox;
    }
}

/* ========================================================================== */

char exibeMenu(void){
	char escolhaUser;
	printf("\t\t\tMENU PRINCIPAL\n\n\n");
	printf("\t\t\t\tOpcoes:\n\n");
	printf("\t\t\t1. Adicionar elemento\n");
	printf("\t\t\t2. Remover o primeiro elemento\n");
	printf("\t\t\t3. Imprimir a lista\n");
	printf("\t\t\t4. Fechar o programa\n\n");
	printf("\t\t\t\tPara escolher uma opcao, entre com o NUMERO CORRESPONDENTE.\n\n");
	printf("\t\t\tESCOLHA: ");
	fflush(stdin);
	escolhaUser = getchar();
	return escolhaUser;
}

TipoItem alimentaLista(){
	TipoItem itemRetorno;
	printf("Entre com a chave do elemento: ");
	scanf("%d", &itemRetorno.Chave);
  printf("Entre com o nome: ");
  fflush(stdin);
  gets(itemRetorno.Nome);
  return itemRetorno;
}

int main(int argc, char *argv[])
{
	system("cls");
  TipoItem item;
	TipoLista lista;
  TipoApontador posicao;
	char escolha = '0';
	FLVazia(&lista); //inicializa a lista
	while(escolha!='5'){
		escolha = exibeMenu();
    system("cls");
		if(escolha == '1'){
      item = alimentaLista();
      Insere(item, &lista); //insere o item na lista
		}
    else if(escolha == '2'){
      posicao = lista.Primeiro;
      Retira(posicao, &lista, &item);
    }
    else if(escolha == '3'){
      Imprime(lista);
    }
    else if(escolha == '4'){
      break;
    }
    else{
      printf("\t\tOpcao INVALIDA!\n\n");
    }
    system("pause");
    system("cls");
	}
}
