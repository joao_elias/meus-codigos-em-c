/*
	Name: Matriz do fatorial
	Copyright: JF Software LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 18/10/21 18:06
	Description: Le um vetor, calcula o fatorial dos
	valores lidos e atribui a outro vetor. 
*/

#include <stdio.h>
#define TAM 15

int main()
{
    double matA[TAM], matB[TAM];
    int i, j;
    for (i=0; i<TAM; i++) {
        printf("Elemento [%d]>>", i);
        scanf("%lf", &matA[i]); //leitura da matriz A
        matB[i] = 1;
        for(j=matA[i]; j>0; j--){
            matB[i] = matB[i]*j; //calculo do fatorial na segunda matriz
        }
    }
    printf("Matriz A:\n\n[ ");
    for(i=0; i<TAM; i++){
        printf("%.0lf ", matA[i]);
    }
    printf("]\n\nMatriz B:\n\n[ ");
    for(i=0; i<TAM; i++){
        printf("%.0lf ", matB[i]);
    }
    printf("]");
    return 0;
}
