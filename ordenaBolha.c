/*
	Name: Bubble Sort
	Copyright: JF SOFTWARE LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 11/11/21 18:03
	Description: Implemente o algoritmo Bubble Sort.
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int TAM, i, j, aux;
    printf("Informe o TAMANHO DO VETOR>>");
    scanf("%d", &TAM);
    int* vct = (int *)malloc(TAM*sizeof(int));
    if(vct == NULL){
        printf("Erro ao criar o vetor!\n\n");
    }
    else{
        printf("Vetor criado com sucesso!\nEsta ocupando %ld bytes na memoria\n\n", TAM*sizeof(vct));
        for(i=0; i<TAM; i++){
            printf("Elemento[%d]>>", i+1);
            scanf("%d", &vct[i]);
        }
        printf("Vetor na ordem informada:\n\n|");
        for(i=0; i<TAM; i++){
            printf("%d|", vct[i]);
        }
        printf("\n\nVetor ordenado:\n\n|");
        for(i=0; i<TAM; i++){
            for(j=TAM-1; j>=0; --j){
                if(vct[j-1]>vct[j]){
                    aux = vct[j-1];
                    vct[j-1] = vct[j];
                    vct[j] = aux;
                }
            }
        }
        for(i=0; i<TAM; i++){
            printf("%d|", vct[i]);
        }
        printf("\n\n");
    }
    free(vct);
    return 0;
}
