/*
	Name: Verificador Curioso de Primo
	Copyright: JFSoftware LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 23/09/21 17:29
	Description: Alterar o codigo trabalhado em sala para descobrir se um valor de entrada e primo.
	(Brinde!) Se nao for, mostrar o primeiro primo anterior ao valor de entrada.
*/

#include <stdio.h>
#include <stdlib.h>

int contaDiv(int val);

int main(void){
	int num, qtdDiv=0, i;
	printf("Digite um numero: ");
	scanf("%d", &num);
	if(contaDiv(num) == 2){ //verifica se e primo
		printf("O numero %d E primo\n", num);
	}
	else{ //so executa se NAO FOR PRIMO
		printf("O numero %d NAO E primo\n", num);
		while(num>1){ //aqui decrementa o numero ate achar o primo anterior ao que foi digitado
			num = num - 1;
			if(contaDiv(num) == 2){
				printf("Primo imediatamente anterior: %d\n", num);
				break;
			}
		}
	}
	printf("\nFim do programa!\n");
	system("pause");
	return 0;
}

int contaDiv(int val){
	//conta os divisores
	int qtdDiv=0, i=0;
	for(i=1; i<=val; i++){
		if(val % i == 0){
			qtdDiv++;
		}
	}
	return qtdDiv;
}