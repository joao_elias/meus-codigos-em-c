/*
	Name: Derivada de Polinomio
	Copyright: JF SOFTWARE LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 18/11/21 14:38
	Description: Calcula a derivada de um polinomio de grau menor que 12
*/

#include <stdio.h>
#include<stdlib.h>
#include <math.h>
#define MAX 12

int main()
{
    int grau, i;
    printf("CALCULO DE DERIVADA POLINOMIAL!\n\n");
    while(1==1){
        printf("Informe o grau do polinomio (menor que 12): ");
        scanf("%d", &grau);
        if(grau<MAX){
            float* coefOrig = (float*)malloc((grau+1)*sizeof(float));
            int indicaGrau = grau;
            for(i=0; i<grau+1; i++){
                printf("Termo de grau %d>>", indicaGrau);
                scanf("%f", &coefOrig[i]);
                --indicaGrau;
            }
            printf("Polinomio informado:\n\n\tf(x) = ");
            indicaGrau = grau;
            for(i=0; i<grau+1; i++){
                printf("%.2f x^%d + ", coefOrig[i], indicaGrau);
                --indicaGrau;
            }
            printf("\n\nDerivada:\n\n\tf'(x) = ");
            float* coefDeriva = malloc(grau*sizeof(float));
            indicaGrau = grau;
            for(i=0; i<grau; i++){
                coefDeriva[i] = coefOrig[i] * indicaGrau;
                printf("%.2f x^%d + ", coefDeriva[i], indicaGrau-1);
                --indicaGrau;
            }
            char escolha;
            printf("\n\nDeseja calcular a derivada no ponto? [S/N]>>");
            fflush(stdin);
            escolha = getchar();
            if(escolha == 's'|| escolha == 'S'){
                float ponto, derivada = 0;
                printf("Qual ponto deseja calcular?\nx = ");
                scanf("%f", &ponto);
                indicaGrau = grau;
                for(i=0; i<grau; i++){
                    derivada = derivada + (coefDeriva[i] * pow(ponto,indicaGrau-1));
                    --indicaGrau;
                }
                printf("Valor da derivada em x = %.3f\nf'(%.3f) = %.3f\n\n", ponto, ponto, derivada);
            }
            free(coefOrig);
            free(coefDeriva);
            break;
        }
        else{
            printf("Grau elevado!\n\n");
        }
    }
    return 0;
}
