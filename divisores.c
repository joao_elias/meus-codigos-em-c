/*
	Name: Divisores
	Copyright: JF Software LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 15/10/21 13:41
	Description: Le um numero e imprime seus divisores
*/

#include <stdio.h>

int main()
{
    int num, i;
    printf("Digite um numero: ");
    scanf("%d", &num);
    for(i=1; i<num; i++){
        if(num%i == 0){
            printf("Divisor de %d: %d\n", num, i);
        }
    }
    return 0;
}
