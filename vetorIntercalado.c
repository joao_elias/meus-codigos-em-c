/*
escrever um programa em que ele tem 2 vetor. ele armazena em A
o numero par e em b os numeros impares no final ele cria um
vetor c que mescla os dois de forma intercalada.
*/

#include <stdio.h>
#define TAM 5

int main()
{
    int num, A[TAM], B[TAM], C[2*TAM], iA=0, iB=0;
    while((iA<5) || (iB<5)){
        printf("Digite um valor>>");
        scanf("%d", &num);
        if(num%2 == 0){
            A[iA] = num;
            printf("Valor %d armazenado em A!\n", num);
            iA++;
        }
        else{
            B[iB] = num;
            printf("Valor %d armazenado em B!\n", num);
            iB++;
        }
    }
    printf("Vetores:\n\n Vetor A:\n\n");
    for(iA=0; iA<TAM; iA++){
        printf("%d ", A[iA]);
    }
    printf("\n\nVetor B:\n\n");
    for(iB=0; iB<TAM; iB++){
        printf("%d ", B[iB]);
    }
    printf("\n\nVetor juncao (A + B intercalados):\n\n");
    int cont;
    iA = 0;
    for(cont=0; cont<2*TAM; cont = cont + 2){
        C[cont] = A[iA];
        C[cont + 1] = B[iA];
        printf("%d ", C[cont]);
        iA++;
    }
    return 0;
}
