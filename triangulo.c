/*
	Name: Verificador de Triangulo
	Copyright: JF Software LTDA
	Author: Joao Elias Ferraz Santana
	Date: 06/10/21 16:34
	Description: Verifica se os lados informados formam um triangulo e informa
	o tipo do triangulo
*/

#include <stdio.h>
#include <stdlib.h>
void main(void){
	float A, B, C;
	printf("Entre com os tres lados:\n");
	scanf("%f %f %f", &A, &B, &C);
	if(A<B+C && B<A+C && C<A+B){
		printf("Forma triangulo!\n");
		if(A==B && A==C){
			printf("Esqulatero!\n");
		}
		else if(A==C || A==B || B==C){
			printf("Isosceles!\n");
		}
		else{
			printf("Escaleno!\n");
		}
	}
	else{
		printf("Nao forma triangulo!\n");
	}
	system("pause");
}
