/*
	Name: Sempre Venda Salarios
	Copyright: JF Software LTDA
	Author: Joao Elias Ferraz Santana
	Date: 15/10/21 13:12
	Description: A empresa "Sempre Venda" remunera seus funcionarios com salario fixo
	mais comissao. Construa um programa que leia o salario fixo de um funcionario e a
	quantidade de produtos vendidos. Se o salario ultrapassar 3500 reais, o mesmo deve
	ter a retencao de IR de 10%. Informe o salario a ser recebido no final do mes.
*/

#include <stdio.h>

int main()
{
    int quantidadeProd;
    float sal;
    printf("Informe o salario.\nR$");
    scanf("%f", &sal);
    printf("Quantidade de vendas: ");
    scanf("%d", &quantidadeProd);
    if(quantidadeProd<20){
        sal = 1.1*sal;
    }
    else if(quantidadeProd>=20 && quantidadeProd<=100){
        sal = 1.12*sal;
    }
    else{
        sal = 1.15*sal;
    }
    if(sal>3500.00){
        sal = 0.9*sal;
    }
    printf("Salario no final do mes: R$%.2f\n\n", sal);
    return 0;
}
