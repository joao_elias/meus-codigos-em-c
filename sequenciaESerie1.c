/*
	Name: Serie Maluca
	Copyright: JF Software LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 15/10/21 13:25
	Description: Ler dois valores x e n. Imprima o valor de S
*/

#include <stdio.h>
#include <math.h>

int main()
{
    float S=0;
    int x, n, i;
    printf("Entre com dois valores (x e n):\n");
    scanf("%d %d", &x, &n);
    for(i=1; i<n; i++){
        S = S + i/(pow(x,i));
    }
    printf("Resultado da soma de uma serie: %f\n", S);
    return 0;
}
