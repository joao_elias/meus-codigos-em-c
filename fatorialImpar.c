/*
	Name: Fatorial Impar
	Copyright: JF SOFTWARE LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 13/10/21 16:07
	Description: Calcula o fatorial de um numero impar de 1 ate 10
*/

#include <stdio.h>

int main()
{
    int num, i, fatorial=1;
    for(num=10; num>=1; num--){
        if(num % 2 !=0){
            for(i=num; i>=1; i--){
                fatorial = fatorial * i;
            }
            printf("Fatorial de %d: %d\n", num, fatorial);
            fatorial = 1;
        }
    }
    return 0;
}
