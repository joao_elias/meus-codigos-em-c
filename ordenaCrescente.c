/*
	Name: Ordenador Crescente
	Copyright: 
	Author: Joao Elias Ferraz Santana
	Date: 05/11/21 18:55
	Description: Ordena um vetor em ordem CRESCENTE
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int TAM, i, j, aux;
    int* vct;
    printf("Quantos dados quer armazenar? ");
    scanf("%d", &TAM);
    vct = (int*)malloc(TAM*sizeof(int));
    for(i=0; i<TAM; i++){
        printf("Valor[%d]>>", i+1);
        scanf("%d", &vct[i]);
    }
    printf("\nVETOR INFORMADO:\n|");
    for(i=0; i<TAM; i++){
        printf("%d|", vct[i]);
    }
    printf("\n\nVETOR ORDENADO:\n\n|");
    for(i=0; i<TAM; i++){
        aux = vct[i];
        for(j=i; j<TAM; j++){
            if(vct[j] < aux){
                aux = vct[j];
                vct[j] = vct[i];
                vct[i] = aux;
            }
        }
        printf("%d|", vct[i]);
    }
    printf("\n\n");
    free(vct);
    return 0;
}
