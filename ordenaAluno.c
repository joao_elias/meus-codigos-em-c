/*
	Name: Ordenador de Aluno
	Copyright: 
	Author: Joao Elias Ferraz Santana
	Date: 12/11/21 17:45
	Description: Faca uma funcao que, dado um valor inteiro N 
	positivo, aloque dinamicamente um vetor de Aluno, leia do
	teclado os N pares de valores (Matricula, Nota) e retorne
	o vetor alocado.  Imprima, ao final do programa alunos
	ordenados por nota final.
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct{
    unsigned int Matricula;
    float Nota;
}Aluno;

int main()
{
    int listaSize, i, j;
    unsigned int Matricula_aux;
    float Nota_aux;
    while(1==1){
        printf("Quantos alunos ha na turma?");
        scanf("%d", &listaSize);
        if(listaSize>0){
           break; 
        }
    }
    Aluno listaAlunos[listaSize];
    for(i=0; i<listaSize; i++){
        printf("Matricula>>");
        scanf("%d", &listaAlunos[i].Matricula);
        printf("Nota>>");
        scanf("%f", &listaAlunos[i].Nota);
    }
    printf("*********************\n\nLista ordenada por NOTA:\n\n");
    for(i=0; i<listaSize; i++){
        for(j=listaSize-1; j>=0; --j){
            if(listaAlunos[j-1].Nota > listaAlunos[j].Nota){
                /*
                aux = vct[j-1];
                vct[j-1] = vct[j];
                vct[j] = aux;
                */
                Nota_aux = listaAlunos[j-1].Nota; //ok
                Matricula_aux = listaAlunos[j-1].Matricula; //ok
                listaAlunos[j-1].Nota = listaAlunos[j].Nota; //ok
                listaAlunos[j-1].Matricula = listaAlunos[j].Matricula; //ok
                listaAlunos[j].Matricula = Matricula_aux;
                listaAlunos[j].Nota = Nota_aux;
            }
        }
    }
    for(i=0; i<listaSize; i++){
        printf("Matricula>> %d \n", listaAlunos[i].Matricula);
        printf("Nota>> %.1f \n\n", listaAlunos[i].Nota);
    }
    return 0;
}
