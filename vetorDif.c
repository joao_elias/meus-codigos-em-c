/*
	Name: Leitura
	Copyright: JF Software LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 18/10/21 15:42
	Description: Ler duas matrizes A e B do mesmo tipo com 20 elementos cada.
	Construir uma matriz C, onde cada elemento de C e a subtracao do elemento
	correspondente de A com B. Apresentar a matriz C.
*/


#include <stdio.h>
#include <stdlib.h>
#define TAM 20

int main(void){
	int i, A[TAM], B[TAM], C[TAM];
	for(i=0; i<TAM; i++){
		printf("Elemento de A[%d]>> ", i);
		scanf("%d", &A[i]);
		printf("Elemento de B[%d]>> ", i);
		scanf("%d", &B[i]);
		C[i] = A[i] - B[i];
	}
	printf("VETOR:\n[ ");
	for(i=0; i<TAM; i++){
		printf("%d ", C[i]);
	}
	printf("]\n\n");
	system("pause");
	return 0;
}

