/*
	Name: Palindromo Quatro
	Copyright: JF Software LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 22/10/21 19:11
	Description: Verifica se um numero de 4 digitos e palindromo
*/

#include <stdio.h>

int inverterVal(int a);
void palindromo(int num);

int main(int argc, char const *argv[])
{
    int val;
    printf("Digite um valor de EXATAMENTE 4 digitos.\nValor>>");
    scanf("%d", &val);
    palindromo(val);
    return 0;
}

int inverterVal(int a){
    if(a>=1000 && a<10000){
        int milhar = a/1000;
        a = a-milhar*1000;
        int centena = a/100;
        a = a-centena*100;
        int dezena = a/10;
        a = a-dezena*10;
        int unidade = a/1;
        int invertido = unidade*1000 + dezena*100 + centena*10 + milhar;
        return invertido;
    }
    else
    {
        printf("Impossivel inverter %d!!\nAqui, eu inverto somente valor de 4 DIGITOS\n\n", a);
        return -1;
    }
    
}

void palindromo(int num){
    int entrada = num;
    int invertido = inverterVal(num);
    if(invertido == entrada){
        printf("Os valores %d e %d sao palindromos!\n\n", entrada, invertido);
    }
    else{
        printf("Os valores %d e %d NAO sao palindromos!\n\n", entrada, invertido);
    }
}
