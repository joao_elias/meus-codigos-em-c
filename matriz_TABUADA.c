/*
	Name: Tabuada
	Copyright: 
	Author: Joao Elias, Diego Santos, Joao Vitor, Gabriel Santos, Bruno Blater(?)
	Date: 20/10/21 16:56
	Description: Tabuada de 0 ate 9
*/

#include <stdio.h>

int main(void){
	int i, j;
	int numeros[10][10];
	for(i=0; i<10; i++){
		for(j=0; j<10; j++){
		    numeros[i][j] = i*j;
		}
	}
	printf(" \t");
	for(i=0; i<10; i++){
	    printf("%d\t ", i);
	}
	printf("\n");
	for(i=0; i<10; i++){
		printf("%d\t", i);
		for(j=0; j<10; j++){
		    printf("%d \t", numeros[i][j]);
		}
		printf("\n");
	}
	system("pause");
	return 0;
}
