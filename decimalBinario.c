/*
	Name: Conversor Decimal-Binario
	Copyright: JFSOFTWARE
	Author: Joao Elias Ferraz Santana
	Date: 04/11/21 10:23
	Description: O usuario entra com um valor inteiro e o programa
	devolve o numero convertido para o sistema binario
*/

/*primeiramente, o usuario digita um valor (prioridade dos parenteses) na linha 22
depois, a funcao le e mostra retorna o valor digitado para a funcao de conversao
ordem de execucao: main()->leValorEMostra(void)->decBin(int num)
*/

#include <stdio.h>

int decBin(int num);
int leValorEMostra(void);

int main()
{
    decBin(leValorEMostra());
    printf("\n\n");
    return 0;
}

int leValorEMostra(void){
    int num;
    printf("Digite um valor>>");
    scanf("%d", &num);
    printf("O valor %d em binario e ", num);
    return num;
}

int decBin(int num){
    if(num == 0){
        return 0;
    }
    printf("%d", num%2);
    return decBin(num/2);
}
