/*
	Name: Soma e Verifica Divisores
	Copyright: 
	Author: Joao Elias Ferraz Santana
	Date: 04/11/21 18:02
	Description: Dado um valor lido do teclado, exibir a soma de todos os
	numeros entre 0 e esse numero. Depois o programa deve exibir os
	divisores desse mesmo numero. As funcoes devem ser implementadas de
    forma recursiva.
*/

#include <stdio.h>

int somaValor(int lim);
void divisores(int valor, int cont);

int main()
{
    int num;
    printf("Digite um valor (limite da soma)\n");
    scanf("%d", &num);
    int soma = somaValor(num);
    printf("Soma de todos os valores ate %d: %d\n\n", num, soma);
    divisores(soma, 1);
    return 0;
}

int somaValor(int lim){
    if(lim == 0){
        return 0;
    }
    if(lim == 1){
        return 1;
    }
    return (lim+somaValor(lim-1));
}

void divisores(int valor, int cont){
    if(cont == valor){
        printf("Divisor: %d\n", valor/cont);
    }
    if(valor%cont == 0){
        printf("Divisor: %d\n", valor/cont);
    }
    divisores(valor, cont+1);
}
