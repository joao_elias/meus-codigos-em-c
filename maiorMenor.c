/*
efetuar a leitura de 5 nr inteiros e informar o maior e o menor valor
P.S.1: N�o devemos usar vetor, loop ou fun��o. Deve-se apenas completar o c�digo para alcan�ar o que se pede
P.S.2: Ao nao usar vetor e loop de repeticao, o codigo fica extenso e com baixa manutenibilidade.
*/

#include<stdio.h>

void main (void){
    int a, b, c, d, e;
    int maior, menor;
    printf("Primeiro valor: ");
    scanf("%d", &a);
    printf("Segundo valor: ");
    scanf("%d", &b);
    printf("Terceiro valor: ");
    scanf("%d", &c);
    printf("Quarto valor: ");
    scanf("%d", &d);
    printf("Quinto valor: ");
    scanf("%d", &e);
    //decisao do maior valor
    if(a>b && a>c && a>d && a>e){
        maior = a;
    }
    else if(b>a && b>c && b>d && b>e){
        maior = b;
    }
    else if(c>a && c>b && c>d && c>e){
        maior = c;
    }
    else if(d>a && d>c && d>b && d>e){
        maior = d;
    }
    else if(e>a && e>c && e>d && e>b){
        maior = e;
    }
    //fim da decisao do maior valor
    //inicio da decisao do menor
    if(a<b && a<c && a<d && a<e){
        menor = a;
    }
    else if(b<a && b<c && b<d && b<e){
        menor = b;
    }
    else if(c<a && c<b && c<d && c<e){
        menor = c;
    }
    else if(d<a && d<c && d<b && d<e){
        menor = d;
    }
    else if(e<a && e<c && e<d && e<b){
        menor = e;
    }
    //fim da desicao do menor
    printf("\nMAIOR VALOR: %d\n\n", maior);
    printf("\nMENOR VALOR: %d\n\n", menor);
}
