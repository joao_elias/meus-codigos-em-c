/*
	Name: Conversor de minuscula pra maiuscula
	Copyright: JF SOFTWARE LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 29/10/21 15:22
	Description: Escreva um programa que leia uma string do teclado e
    converta todos os seus caracteres em maiuscula. Dica: subtraia 32
    dos caracteres cujo codigo ASCII esta entre 97 e 122.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char frase[1024];
    int i;
    printf("Entre com uma frase>>");
    fflush(stdin);
    gets(frase);
    for(i=0; i<strlen(frase); i++){
        if(frase[i]>=97 && frase[i]<=122){
            frase[i] = frase[i]-32;
        }
    }
    printf("Frase com letra MAIUSCULA>> ");
    puts(frase);
    return 0;
}
