/*
Disciplina   : [Linguagem e Logica de Programacao]
Professor   : Orlando Vicente de Oliveira Filho
Descricao   : Converte de Celcius pra Fahrenheit
Autor(a)    : Joao Elias Ferraz Santana
Data atual  : 08/09/2021
*/

#include <stdio.h>
#include <stdlib.h>

int main(void){
    float F, C;
    printf("Entre com a temperatura, em C: ");
    scanf("%f", &C);
    F=(9*C + 160)/5;
    printf("%.1f Celcius, em Fahrenheit, vale %.1f Fahrenheit\n", C, F);
    system("pause");
    return 0;
}