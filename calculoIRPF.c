/*
Desenvolver o códido em C que receba do teclado um determinado valor
de salário e informe o valor do IR (Imposto de renda) devido. O cálculo
do imposto é feito de forma escalonada sob a seguinte regra:
--Salário até 2000,00 o imposto é isento.
--Salário até 3000,00 o imposto será de 8%.
--Salário até 4500,00 o imposto será de 18%.
--Salário acima de 4500,00 o imposto será de 28%.
OBSERVEM que é IMPRESCINDÍVEL considerar todas as faixas cabíveis de imposto.
Sigam os exemplo:
*Salário de 2000,00 deve aparecer na tela que o imposto é ISENTO.
*Salário de 2500,00 deve cobrar imposto SOBRE os 500 excedentes, já que 2000 é
isento, ou seja: imposto será de 500 * 0.08, = 40,00.
*Salário de 3500 deve considerar 2000 isento, 8% sobre 1000 e 18% sobre 500,
ou seja: (1000 * 0,08) + (500 * 0,18) = 80 + 90=170
*Salário de 9800 deve considerar 2000 isento, 8% sobre 1000, 18% sobre 1500
e 28% sobre o restante.
O programa deve perguntar qual o salário infinitamente até que o salário
informado seja negativo. Neste momento o programa deve informar quantos
salários foram informados e qual a soma do imposto total recolhido.
*/
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    float salario, irpf, totalIRPF=0;
    int verdade = 1, qSalario = 0;
    while(verdade == 1){
        system("cls");
        printf("**********\tCALCULO DO IRPF\t**********\n\n\n\n");
        printf("Informe seu SALARIO (valor negativo encerra).\nR$: ");
        scanf("%f", &salario);
        if(salario<0){
            break;
        }
        else if (salario<=2000)
        {
            irpf = 0;
            printf("ISENTO DE IMPOSTO.\n\n");
        }
        else if (salario>2000 && salario<=3000)
        {
            irpf = 0*salario;
            salario = salario - 2000;
            irpf = 0.08*salario;
            printf("Imposto: R$%.2f\n", irpf);
        }
        else if (salario>3000 && salario<=4500)
        {
            salario = salario - 2000;
            irpf = 0.08*1000 + 0.18*(salario-1000);
            printf("Imposto: R$%.2f\n", irpf);
        }
        else if (salario>4500)
        {
            salario = salario - 2000;
            irpf = 0.08*1000;
            salario = salario - 1000;
            irpf = irpf + 0.18*1500;
            salario = salario - 1500;
            irpf = irpf + 0.28*salario;
            printf("Imposto: R$%.2f\n", irpf);
        }
        totalIRPF = totalIRPF + irpf;
        system("pause");
        system("cls");
        qSalario++;
    }
    printf("**********\tBALANCO DO IRPF\t**********\n\n\n\n");
    printf("TOTAL DE SALARIOS: %d\n\n", qSalario);
    printf("TOTAL DE IMPOSTOS RECOLHIDOS: R$%.2f\n\n", totalIRPF);
    system("pause");
    return 0;
}
