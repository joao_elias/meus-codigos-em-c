/*
Em um parque, o numero de pessoas que pode entrar varia de acordo com a
capacidade de lanches servidos. Por isso esse valor pode variar diariamente.
Tambem existe como limitador, a altura maxima e a altura minima de cada
participante.
Crie um programa que possua uma estrutura que armazene a qtd, altMax, altMin
e que controle a quantidade de pessoas dentro do parque. A quantidade de
pessoas varia para cada um que entrar, estando abaixo da altMax, acima da altMin
e nao pode exceder o numero maximo de pessoas.
O programa vai receber inicialmente a quantidade maxima do dia, a altMax em metros
e a altMin em metros.
Depois o programa vai entrar em um loop infinito perguntando a altura do candidato
a entrar no parque, se for apto, ele vai informar apto e contar mais um para dentro,
senao, ele informara inapto e ignorara o valor.
Apos alcancar a lotacao maxima, o programa vai informar na tela e nao deixara entrar
mais ninguem. Nesse momento o programa fecha.
*/

/*
	Name: Controle de pessoas
	Copyright: JF SOFTWARE LTDA
	Author: Joao Elias Ferraz Santana
	Date: 23/11/21 16:22
	Description: Um controle de entrada para um parque.
*/


#include <stdio.h>

typedef struct{
    int maxPessoa;
    float altMax;
    float altMin;
    int aptos;
}Parque;

int main()
{
    Parque parque;
    int i;
    float alt;
    parque.aptos=0;
    printf("Qual a capacidade maxima pra hj?");
    scanf("%d", &parque.maxPessoa);
    printf("Altura maxima (em m): ");
    scanf("%f", &parque.altMax);
    printf("Altura minima (em m): ");
    scanf("%f", &parque.altMin);
    for(i=1; i<=parque.maxPessoa; i++){
        printf("Sua altura (em m)>>");
        scanf("%f", &alt);
        if(alt>=parque.altMin && alt<=parque.altMax){
            parque.aptos++;
            printf("Apto(a)!\n");
        }
        else{
            printf("NAO pode entrar!\n");
        }
        if(i == parque.maxPessoa){
            printf("Parque LOTADO!!\n\n");
        }
    }
    printf("Para o parque, estao aptos %d pessoas\n\n", parque.aptos);
    return 0;
}
