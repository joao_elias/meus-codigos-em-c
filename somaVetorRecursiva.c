/*
	Name: Somador de Vetor
	Copyright: JF SOFTWARE LTDA.
	Author: Joao Elias Ferraz Santana
	Date: 04/11/21 09:58
	Description: Usando recursividade, calcule a soma de todos os 
    valores de um array de reais.
*/

#include <stdio.h>
#include <stdlib.h>

float somaVetor(float vct[], int size);

int main(int argc, char const *argv[])
{
    int TAM, i;
    printf("Quantos numeros vc quer guardar? ");
    scanf("%d", &TAM);
    printf("Sera criado um vetor de %d posicoes\n\n", TAM);
    float* vetor;
    vetor = (float *)malloc(TAM*sizeof(float));
    for (i = 0; i < TAM; i++)
    {
        printf("Elemento[%d]>>", i+1);
        scanf("%f", &vetor[i]);
    }
    printf("Soma de todos os elementos do vetor>> %.2f\n\n", somaVetor(vetor, TAM));
    free(vetor);
    return 0;
}

float somaVetor(float vct[], int size){
    if (size == 0)
    {
        return 0;
    }
    
    return vct[size-1] + somaVetor(vct, size-1);
}
