/*
	Name: Buscador
	Copyright: 
	Author: Joao Elias Ferraz Santana
	Date: 27/10/21 19:20
	Description: Cadastra N pessoas e busca por alguma delas
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define QPESSOAS 20
#define MAXNOME 255

int main()
{
    char pessoa[QPESSOAS][MAXNOME], nomePesquisa[MAXNOME], pesquisar = 's';
    int i, j, achou = 0, posicao;
    for(i=0; i<QPESSOAS; i++){
        printf("Nome da %d pessoa: ", i+1); //leitura das 20 pessoas.
        fflush(stdin);
        gets(pessoa[i]);
    }
    while(pesquisar == 's'|| pesquisar == 'S'){
        printf("Digite uma pessoa para procurar na base de dados.\nEstao armazenadas %d pessoas.\nNOME>>", QPESSOAS);
        fflush(stdin);
        gets(nomePesquisa);
        for(i=0; i<QPESSOAS; i++){
            for(j=0; j<strlen(pessoa[i]); j++){
                if(nomePesquisa[i]==pessoa[i][j]){
                    achou = 1;
                    posicao = i;
                }
                else{
                    achou = 0;
                    posicao = -1;
                }
            }
        }
        if(posicao != -1 && achou == 1){
            printf("A pessoa existe! Foi cadastrada na posicao %d\n", posicao);
        }
        else{
            printf("Pessoa inexistente!\n");
        }
        printf("Deseja continuar procurando? [S/N]");
        fflush(stdin);
        pesquisar = getchar();
        if(pesquisar == 'n'|| pesquisar == 'N'){
            break;
        }
        fflush(stdin);
    }
    return 0;
}
