/*
	Name: Numeros Perfeitos 
	Copyright: JF SOFTWARE
	Author: Joao Elias Ferraz Santana
	Date: 28/10/21 16:23
	Description: Escrever um programa que leia 5 números do teclado e informe seus
	divisores, exceto o próprio número.  Após isso, mostrar quais números são perfeitos
	e para esses, informar também o seu quadrado.
	Exemplo: Para o valor 8: 1+2+4 = 7  não é perfeito
	6 = 1 + 2 + 3 número perfeito, logo deve informar 36 também.
*/

#include <stdio.h>
#include <stdlib.h>
#define QNUM 5

int somaDivisor(int a);
void verificaPerfeito(int valor);

int main()
{
    int num, cont;
    for(cont = 1; cont<=QNUM; cont++){
		printf("Digite um valor>>");
    	scanf("%d", &num);
    	verificaPerfeito(num);
	}
    system("pause");
    return 0;
}

int somaDivisor(int a){
    int i, acum=0;
    for(i=1; i<a; i++){
        if(a % i == 0){
            acum = acum + i;
        }
    }
    return acum;
}

void verificaPerfeito(int valor){
    if(somaDivisor(valor) == valor){
        printf("O numero %d e PERFEITO!\n\n", valor);
        printf("Quadrado de %d: %d", valor, valor*valor);
    }
    else{
        printf("O numero %d NAO e PERFEITO!\n\n", valor);
    }
}
